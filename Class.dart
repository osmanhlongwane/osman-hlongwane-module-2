//a) then use an object to print the name of the app, sector/category, developer,
//and the year it won MTN Business App of the Year Awards.
class Mtnapp {
  var appname;
  var appcategory;
  var appdeveloper;
  var appyear;

  //b) Create a function inside the class,
  //transform the app name to all capital letters and then print the output.

  // defining class function
  CapLetters() {
    print("App Name in Capital Letters : ${appname.toUpperCase()}");
  }
}

void main() {
  // Creating Object called App
  var App = new Mtnapp();
  App.appname = "wumdrop";
  App.appcategory = "interprise";
  App.appdeveloper = "simon hartley";
  App.appyear = 2015;

  print("App name is ${App.appname}");
  print("App category is ${App.appcategory}");
  print("App developer is ${App.appdeveloper}");
  print("App year is ${App.appyear}");

  App.CapLetters();
}
